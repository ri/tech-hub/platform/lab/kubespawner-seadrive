'''
Utilities for the KubeSpawner configuration.
'''

import configparser
import io
import re

from kubernetes.client.models import (
    V1ObjectMeta,
    V1Secret
)
import kubernetes.client.rest
from kubespawner import KubeSpawner
from tornado.httpclient import HTTPRequest, AsyncHTTPClient, HTTPClientError
from traitlets import Unicode, Int
from .config import DEFAULT_DRIVE_CONFIG


class KuJuDriveSpawner(KubeSpawner):
    '''The Kubernetes Jupyterhub Drive spawner launches a seadrive sidecar
    container in the user's notebook pod to mount the user's
    libraries.
    '''
    def generate_drive_config(self, url, username, token, **kwargs):
        base = DEFAULT_DRIVE_CONFIG.copy()
        base.update(**kwargs)
        cp = configparser.ConfigParser()
        cp.read_dict(base)
        cp['account'].update(dict(server=url, username=username, token=token))
        repo_name = self.extract_repo_name()
        if repo_name: # spawned with link to a specific repo
            cp.add_section('repo') # add a custom seafile config option which makes sure default repo is initialized first
            cp['repo'].update(dict(default_repo=repo_name))
        cfg = io.StringIO()
        cp.write(cfg)
        cfg.seek(0)
        data = {
            'seadrive.conf': cfg.read()
        }
        metadata = V1ObjectMeta(
            name='seadrive-{username}-conf'.format(username=username),
            annotations={'username': username})
        secret = V1Secret(string_data=data, metadata=metadata)
        return secret

    def extract_repo_name(self):
        '''Only applicable for shared links. For shared links the repo name can be extracted from the link url
        '''
        try:
            query_arguments = self.handler.request.query_arguments
            for _, byte_list in query_arguments.items():
                for byte_string in byte_list:
                    decoded_query_arg = byte_string.decode('utf8')
                    repo_name_search = re.search('lab\/tree\/shared\/([^\/|?|&]*)', decoded_query_arg, re.IGNORECASE)
                    if repo_name_search:
                        repo_name = repo_name_search.group(1)
                        self.log.debug("Extracted repo_name {repo_name}".format(repo_name=repo_name))
                        return repo_name
        except Exception as e:
            self.log.warning("Failed to extract repo name.\n" +
                             "Exception: {e}".format(e=e))
        return None

    async def options_form(self, spawner):
        '''Dynamically customize spawner profile list based on user
        '''    
        user = spawner.user
        auth_state = await user.get_auth_state()
        user_groups = auth_state.get('oauth_user', {}).get('roles', {}).get('group', [])
        if self.dynamic_memory_group in user_groups: # is user is allowed to use high memory profile
            mem_limit = self.memory_upper_limit
            mem_limit_gb = mem_limit/1024**3
            spawner.log.debug(f'Set higher memory limit ({mem_limit_gb}GB) for {spawner.user.name}')
            high_mem_profile = {
                'display_name': f'High Memory Profile ({mem_limit_gb}GB)',
                'kubespawner_override': {
                    'id': 'high_mem_profile',
                    'mem_limit': mem_limit,
                    'image_spec': self.dynamic_memory_image # can be configured in config file. Could also be removed if the default image is to be used
                }
            }
            if not next((x for x in spawner.profile_list if x.get('kubespawner_override', {}).get('id', '') == 'high_mem_profile'), None):
                # only add this if wasn't added before
                spawner.profile_list.append(high_mem_profile)
        # Let KubeSpawner inspect profile_list and decide what to return
        #
        # ref: https://github.com/jupyterhub/kubespawner/blob/37a80abb0a6c826e5c118a068fa1cf2725738038/kubespawner/spawner.py#L1885-L1935
        return spawner._options_form_default()

    
    async def pre_spawn_hook(self, spawner):
        user = spawner.user
        spawner.log.debug("Running pre spawn hook for %s" % user.name)
        auth_state = await user.get_auth_state()
        drive_token = await self.get_drive_token(auth_state)
        secret = self.generate_drive_config(self.drive_url, user.name, drive_token)
        try:
            secret_name = 'seadrive-{username}-conf'.format(username=user.name)
            spawner.api.patch_namespaced_secret(secret_name, self.namespace, secret)
            spawner.log.debug("Updated secret for %s" % user.name)
        except kubernetes.client.rest.ApiException as e:
            if e.status == 404: # not found
                pass
            else:
                raise
        try:
            spawner.api.create_namespaced_secret(self.namespace, secret)
            spawner.log.debug("Created secret for %s" % user.name)
        except kubernetes.client.rest.ApiException as e:
            if e.status == 409:
                pass
            else:
                raise
        return user

    async def get_drive_token(self, auth_state: dict) -> str:
        client = AsyncHTTPClient()
        request = HTTPRequest(
            self.drive_url + '/api2/account/token/',
            headers={'Authorization': 'Bearer {token}'.format(
                token=auth_state['access_token'])},
            connect_timeout=1.0,
            request_timeout=self.request_timeout_drive_api # use the configurable timeout here
        )
        try:
            resp = await client.fetch(request)
            token = resp.body.decode('utf-8')
        except HTTPClientError as e:
            self.log.warning("Failed to obtain drive token for user {username}.\n" +
                             "Exception: {e}".format(username=self.user.name, e=e))
            e.message = 'Your Drive is not initialized yet, please visit {drive_url} to enable your drive account'.format(drive_url=self.drive_url)
            raise
        return token

    drive_url = Unicode(
        allow_none=False,
        config=True,
        help='The url of the Seafile server. eg https://drive.example.com',
    )

    seadrive_sidecar_image = Unicode(
        help='The seadrive sidecar image name and tag.',
        default_value='',
        config=True,
    )

    dynamic_memory_group = Unicode(
        help='The group name that enables access to dynamic memory allocation',
        default_value='group-app-collaboratory-lab--dynamic-memory',
        config=True,
    )

    memory_upper_limit = Int(
        help='The upper limit for dynamic memory allocation',
        default_value=8589934592, # limit in Bytes (8GB)
        config=True,
    )

    dynamic_memory_image = Unicode(
        help='The image spec that is used in the high memory profile',
        default_value='clb-jupyter-image/base:1.4',
        config=True,
    )

    request_timeout_drive_api = Int(
        default_value=3,
        config=True,
        help='The timeout for HTTP requests to the Seafile server API, in seconds.',
    )
